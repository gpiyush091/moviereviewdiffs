package featureSet;

public class EntityFeatures {
	String entityName;
	String[] attributes;
	String score;

	public EntityFeatures(String name, String[] attr, String sc) {
		entityName = name;
		attributes = attr;
		score = sc;

	}

	public String[] getEntityFeatures() {

		return attributes;
	}

	public String getScore() {

		return score;
	}
}

/*
 * public ArrayList<Float> getFeatures(String filePath) {
 * 
 * DocumentPreprocessor dp = new DocumentPreprocessor(filePath); //String
 * link="http://cogcomp.cs.illinois.edu/demo/ner/?id=8"; //radiology server
 * String link="http://cogcomp.cs.illinois.edu/demo/wikify/?id=25"; WebDriver
 * driver = new HtmlUnitDriver(); String pageHead=null; driver.get(link);
 * 
 * String process="";
 * 
 * for (List<HasWord> sentence : dp) { for (HasWord word: sentence) {
 * process+=word.toString(); process+=" ";
 * 
 * } } System.out.println("string hai  "+process.toString());
 * driver.findElement(By.id("text")).clear();
 * driver.findElement(By.id("text")).sendKeys(process); //
 * driver.findElement(By.
 * id("tbEncId")).sendKeys(request.getParameter("encntr_id"));
 * driver.findElement(By.cssSelector("input[type='submit']")).click();
 * 
 * System.out.println("ff "+driver.getTitle());
 * //driver.get("http://cogcomp.cs.illinois.edu/demo/ner/results.php");
 * 
 * //String queryResult= driver.findElement(By.id("results")).getText();
 * ListIterator it = driver.findElements(By.className("wiki")).listIterator();
 * while(it.hasNext()){ System.out.println("gggg "+ it.next().toString());
 * 
 * 
 * } //System.out.println("dfdfd "+queryResult);
 * //System.out.println("dkfjdk "+process.toString()); for (HasWord word:
 * sentence) {
 * 
 * System.out.println("word " + word.toString());
 * 
 * 
 * 
 * }
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * return null;
 * 
 * } public static void main(String[] args){ FileManager fm = new FileManager();
 * 
 * EntityFeatures ef = new EntityFeatures();
 * 
 * 
 * String dirPath = "/Users/chaitanya/Documents/workspaceTemp/NLP/test/good";
 * ArrayList<String> filesList = fm.getFileList(dirPath); //for (int i=0;
 * i<filesList.size();i++) {
 * 
 * //}
 * 
 * 
 * ef.getFeatures(filesList.get(1));
 * 
 * }
 */

