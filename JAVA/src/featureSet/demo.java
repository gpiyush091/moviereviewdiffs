package featureSet;

import java.util.ArrayList;
import java.util.HashMap;

import LBJ2.nlp.SentenceSplitter;
import LBJ2.nlp.WordSplitter;
import LBJ2.nlp.seg.PlainToTokenParser;
import LBJ2.nlp.seg.Token;
import edu.illinois.cs.cogcomp.lbj.chunk.Chunker;

public class demo {

	public static void main(String[] args) {
		Chunker tagger = new Chunker();
		//String testfile = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\hunger_games_amazon\\B00I2TW0UO_0";
		//String testfile = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\hunger_games_2013_rt\\Alonso Duralde.txt";
		String testfile = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\tmp\\tmp.txt";
		PlainToTokenParser parser =
				new PlainToTokenParser(
						new WordSplitter(
								new SentenceSplitter(testfile)));

		// HashMap<String,ArrayList<String>> output = new HashMap<String,ArrayList<String>>();
		// ArrayList<String> simpleOutput = new ArrayList<String>();
		Token word;

		String previous = "";
		
		ArrayList<String> adjective = new ArrayList<>();
		ArrayList<String> nounPhrase = new ArrayList<>();
		Boolean anaphora = false;
		Boolean flagForNP = false;
		StringBuilder nounphrase = new StringBuilder();
		
		HashMap<String, Float> POSFrequency = new HashMap<>();
		
		while((word = (Token) parser.next()) != null)
		{
			String prediction = tagger.discreteValue(word);
			if (prediction.startsWith("B-") || prediction.startsWith("I-")
					&& !previous.endsWith(prediction.substring(2))) {
				System.out.print("[" + prediction.substring(2) + " ");
				if (anaphora && nounphrase.length()>0 && !prediction.substring(2).contains("NP")) {
					anaphora = false;
				} else if (anaphora && prediction.substring(2).contains("NP")) {
					// implies NP has not been populated. Start populating NP until we exhaust on consecutive NP
					flagForNP = true;
				} else if (anaphora && flagForNP && !prediction.substring(2).contains("NP")) {
					flagForNP = false;
					anaphora = false;
					nounPhrase.add(nounphrase.toString());
					nounphrase = new StringBuilder();
				}
			}
			
			System.out.print("(" + word.partOfSpeech + " " + word.form + ") ");
			
			if (word.partOfSpeech.equals(".") && word.form.equals(".")) {
				anaphora = false;
				if (nounphrase.length()>0 ) {
					nounPhrase.add(nounphrase.toString());
					nounphrase = new StringBuilder();
				}
				flagForNP = false;
			}
			if (flagForNP) {
				if (word.partOfSpeech.contains("NN")) {
					nounphrase.append(word.form);
					nounphrase.append(" : ");	
				}
			}
			if (anaphora) {
				if (prediction.equals("O")) {
					nounPhrase.add(nounphrase.toString());
					nounphrase = new StringBuilder();
					anaphora = false;
				} else if (prediction.substring(2).contains("NP") && !flagForNP) {
					if (word.partOfSpeech.contains("NN")) {
						nounphrase.append(word.form);
						nounphrase.append(" : ");	
					}
				}
			}
			
			if (word.partOfSpeech.contains("JJ")) {
				if (anaphora) {
					nounPhrase.add(nounphrase.toString());
					nounphrase = new StringBuilder();
				}
				adjective.add(word.form);
				anaphora = true;
				/*
				if (prediction.substring(2).contains("NP")) {
					anaphora = true;
				}
				*/
			}
			if (!prediction.equals("O") && (word.next == null
					|| tagger.discreteValue(word.next).equals("O")
					|| tagger.discreteValue(word.next).startsWith("B-")
					|| !tagger.discreteValue(word.next)
					.endsWith(prediction.substring(2)))) {
				System.out.print("] ");
				if (!anaphora && nounphrase.length()>0) {
					nounPhrase.add(nounphrase.toString());
					nounphrase = new StringBuilder();
				}
			}
			if (word.next == null) {
				System.out.println();
				// System.out.println("Next sentence");
				anaphora = false;
			}
			previous = prediction;
			//System.out.println("prediction:"+prediction);
			String tag = tagger.discreteValue(word);
			
		    String x = word.toString();
		    String key = x.substring(x.indexOf(" ")+1, x.lastIndexOf(" "));
		    String val = x.substring(x.lastIndexOf(" ")+1, x.lastIndexOf(")"));
		    System.out.println("key:"+key+" --val: "+val);
		    
		    if (POSFrequency.containsKey(key)) {
		    	POSFrequency.put(key, POSFrequency.get(key)+1.0f);
		    } else {
		    	POSFrequency.put(key, 1.0f);
		    }
		    
		    //simpleOutput.add("("+key+","+val+")");
			
		}
		
		//nounPhrase.add(nounphrase.toString());
		
		for (int i=0; i<adjective.size(); i++) {
			System.out.println("adjective: "+adjective.get(i));
		}
		
		for (int i=0; i<nounPhrase.size(); i++) {
			System.out.println("noun: "+nounPhrase.get(i));
			//PlainToTokenParser parser1 = new PlainToTokenParser(new WordSplitter(new SentenceSplitter(nounPhrase.get(i))));
		}
	}
}
