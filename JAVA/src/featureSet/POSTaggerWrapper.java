package featureSet;

import java.util.HashMap;

import edu.illinois.cs.cogcomp.lbj.chunk.Chunker;
import LBJ2.nlp.SentenceSplitter;
import LBJ2.nlp.WordSplitter;
import LBJ2.nlp.seg.PlainToTokenParser;
import LBJ2.nlp.seg.Token;

public class POSTaggerWrapper {
	@SuppressWarnings("unused")
	public HashMap<String, Float> getPOSFrequencyForDocument(String testfile) {
		PlainToTokenParser parser = new PlainToTokenParser(new WordSplitter(
				new SentenceSplitter(testfile)));

		Token word;
		HashMap<String, Float> POSFrequency = new HashMap<>();

		POSFrequency.put("#", 0.0f);
		POSFrequency.put("$", 0.0f);
		POSFrequency.put("\"", 0.0f);
		POSFrequency.put("\"", 0.0f);
		POSFrequency.put(",", 0.0f);
		POSFrequency.put("-LRB-", 0.0f);
		POSFrequency.put("-RRB-", 0.0f);
		POSFrequency.put(".", 0.0f);
		POSFrequency.put(":", 0.0f);
		POSFrequency.put("CC", 0.0f);
		POSFrequency.put("CD", 0.0f);
		POSFrequency.put("DT", 0.0f);
		POSFrequency.put("EX", 0.0f);
		POSFrequency.put("FW", 0.0f);
		POSFrequency.put("IN", 0.0f);
		POSFrequency.put("JJ", 0.0f);
		POSFrequency.put("JJR", 0.0f);
		POSFrequency.put("JJS", 0.0f);
		POSFrequency.put("LS", 0.0f);
		POSFrequency.put("MD", 0.0f);
		POSFrequency.put("NN", 0.0f);
		POSFrequency.put("NNP", 0.0f);
		POSFrequency.put("NNPS", 0.0f);
		POSFrequency.put("NNS", 0.0f);
		POSFrequency.put("PDT", 0.0f);
		POSFrequency.put("POS", 0.0f);
		POSFrequency.put("PRP", 0.0f);
		POSFrequency.put("PRP$", 0.0f);
		POSFrequency.put("RB", 0.0f);
		POSFrequency.put("RBR", 0.0f);
		POSFrequency.put("RBS", 0.0f);
		POSFrequency.put("RP", 0.0f);
		POSFrequency.put("SYM", 0.0f);
		POSFrequency.put("TO", 0.0f);
		POSFrequency.put("UH", 0.0f);
		POSFrequency.put("VB", 0.0f);
		POSFrequency.put("VBD", 0.0f);
		POSFrequency.put("VBG", 0.0f);
		POSFrequency.put("VBN", 0.0f);
		POSFrequency.put("VBP", 0.0f);
		POSFrequency.put("VBZ", 0.0f);
		POSFrequency.put("WDT", 0.0f);
		POSFrequency.put("WP", 0.0f);
		POSFrequency.put("WP$", 0.0f);
		POSFrequency.put("WRB", 0.0f);

		Chunker tagger = new Chunker();
		
		while ((word = (Token) parser.next()) != null) {
			
			String tag = tagger.discreteValue(word);
		    // System.out.println(word.toString() + " --> " + tag);
		    
		    String x = word.toString();
		    String key = x.substring(x.indexOf(" ")+1, x.lastIndexOf(" "));
		    String val = x.substring(x.lastIndexOf(" ")+1, x.lastIndexOf(")"));
		    
		    if (POSFrequency.containsKey(key)) {
				POSFrequency.put(key, POSFrequency.get(key) + 1.0f);
			} else {
				POSFrequency.put(key, 1.0f);
			}
		}
		
		return POSFrequency;
	}
}
