package featureSet;

import java.util.ArrayList;

import LBJ2.nlp.Sentence;
import LBJ2.nlp.SentenceSplitter;
import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.PorterStemmer;
import edu.cmu.lti.ws4j.util.StopWordRemover;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;

public class SentenceSimilarity {

	private ArrayList<String> sentences = new ArrayList<>();

	// For each document construct a sentence similarity score.
	public float getSimilarityScoreForDocument(String docPath, int sentenceCount) {
		// TODO Auto-generated constructor stub
		SentenceSplitter sp = new SentenceSplitter(docPath);
		Sentence s;
		while ((s = (Sentence) sp.next()) != null) {
			sentences.add(s.toString());
		}

		float avgSimilarityScore = 0.0f;
		
		for (int i = 0; i < sentences.size(); i++) {
			
			String sentence1 = sentences.get(i);
			for (int j = i + 1; j < sentences.size(); j++) {
				
				if (avgSimilarityScore == 0.0f) {
					avgSimilarityScore = getSentenceSimilarity(sentence1,
							sentences.get(j));
				} else {
					avgSimilarityScore = (avgSimilarityScore + getSentenceSimilarity(
							sentence1, sentences.get(j))) / 2;
				}
			}
		}
		return avgSimilarityScore;
	}

	private static ILexicalDatabase db = new NictWordNet();
	//private RelatednessCalculator rc = new HirstStOnge(db);
	private RelatednessCalculator rc = new WuPalmer(db);
	
	public float getSentenceSimilarity(String sentence1, String sentence2) {
		// Test ws4j sentence vs sentence

		WS4JConfiguration.getInstance();
		float sum = 0.0f;
		if (sentence1 != null && sentence2 != null) {
			
			PorterStemmer ps = new PorterStemmer();
			sentence1 = ps.stemSentence(sentence1);
			sentence2 = ps.stemSentence(sentence2);
			
			String[] arr1 = StopWordRemover.getInstance().removeStopWords(sentence1.split(" "));
			String[] arr2 = StopWordRemover.getInstance().removeStopWords(sentence2.split(" "));

			double[][] matrix = null;

			//System.out.println(arr1.length+" : "+arr2.length);
			try {
				matrix = rc.getSimilarityMatrix(arr1, arr2);
			} catch (NullPointerException e) {
				System.out.println("Null in here" + rc);
			}

			for (int i = 0; i < matrix.length; i++) {
				for (int j = 0; j < matrix[0].length; j++) {
					if (matrix[i][j] == -1.0) {
						matrix[i][j] = 0.0f;
					}
					matrix[i][j] = matrix[i][j];
					sum += (float)matrix[i][j];
				}
			}
			float netCells = ((float) (matrix.length * matrix[0].length));
			sum = sum / netCells;
			
		}
		
		
		if (Double.isInfinite(sum) || Double.isNaN(sum)) {
			sum = 0.0f;
		}
		return sum;
	}
}
