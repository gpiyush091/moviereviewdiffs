package featureSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.DocumentPreprocessor;

public class Features {
	
	public ArrayList<Float> getFeatures(String filePath) {
		
		System.out.println(filePath);
		ArrayList<Float> featureSet = new ArrayList<>();
		/*
		 * Character-based features
		 * f1 Total number of characters(C)
		 * f2 Total number of alphabetic characters/C
		 * f3 Total number of upper-case characters/C
		 * f4 Total number of digit characters/C
		 * f5 Total number of white-space characters/C
		 * f6 Total number of tab spaces/C
		 * f7�f32 Frequency of letters (26 features) - A�Z
		 * f33�f53 Frequency of special characters (21 features) -  ~ , @, #, $, %, ^, &, *, -, _, = ,+, >, <, [, ], {, }, /, \, |
		 * 
		 * 54. Total number of words (M)
		 * 55. Total number of short words (less than four characters)/M e.g., and, or
		 * 56. Total number of characters in words/C
		 * 57. Average word length
		 * 58. Average sentence length in terms of character
		 * 59. Average sentence length in terms of word
		 * 60. Total different words/M
		 * 61. Hapax legomena* Frequency of once-occurring words
		 * 62. Hapax dislegomena* Frequency of twice-occurring words
		 * 
		 * 63. Average similarity score of a document.
		 * 
		 * 64. pound sign : "#"
		 * 65. dollar sign : "$"
		 * 66. open double quote : "``"
		 * 67. close double quote : "''"
		 * 68. comma : ","
		 * 69. left bracket : "-LRB-"
		 * 70. right bracket : "-RRB-"
		 * 71. final punctuation : "."
		 * 72. (semi-)colon : ":"
		 * 73. coordinating conjunction : "CC"
		 * 74. cardinal number : "CD"
		 * 75. determiner : "DT"
		 * 76. existential there : "EX"
		 * 77. foreign word : "FW"
		 * 78. preposition : "IN"
		 * 79. adjective : "JJ"
		 * 80. comparative adjective : "JJR"
		 * 81. superlative adjective : "JJS"
		 * 82. list item marker : "LS"
		 * 83. modal : "MD"
		 * 84. singular noun : "NN"
		 * 85. proper singular noun : "NNP"
		 * 86. proper plural noun : "NNPS"
		 * 87. plural noun : "NNS"
		 * 88. predeterminer : "PDT"
		 * 89. possesive ending : "POS"
		 * 90. possesive pronoun : "PRP"
		 * 91. personal pronoun : "PRP$"
		 * 92. adverb : "RB"
		 * 93. comparative adverb : "RBR"
		 * 94. superlative adverb : "RBS"
		 * 95. particle : "RP"
		 * 96. symbol : "SYM"
		 * 97. to : "TO"
		 * 98. interjection : "UH"
		 * 99. base form verb : "VB"
		 * 100. verb past tense : "VBD"
		 * 101. verb gerund / present participle : "VBG"
		 * 102. verb past participle : "VBN"
		 * 103. verb non 3rd ps. sing. present : "VBP"
		 * 104. verb 3rd ps. sing. present : "VBZ"
		 * 105. wh-determiner : "WDT"
		 * 106. wh-pronoun : "WP"
		 * 107. possesive wh-pronoun : "WP$"
		 * 108. wh-adverb : "WRB"
		 * 
		 * Vocabulary Richness and Readability
		 * 109. Flesh Reading Ease score
		 * 110. Yule's K measure
		 * 111. Simpson's D measure
		 * 112. Sichel's S measure
		 * 113. Brunet's W measure
		 * 114. Honore's R measure
		 * */

		float f1 = 0, f2 = 0, f3 = 0, f4 = 0, f5 = 0, f6 = 0, f7 = 0, 
				f8 = 0, f9=0, f10 = 0, f11 = 0, f12 = 0, f13 = 0, 
				f14 = 0, f15 = 0, f16 = 0, f17 = 0, f18 = 0, f19 = 0, 
				f20 = 0, f21 = 0, f22 = 0, f23 = 0, f24 = 0, f25 = 0, 
				f26 = 0, f27 = 0, f28 = 0, f29 = 0, f30 = 0, f31 = 0, f32 = 0;

		float f33 = 0, f34 = 0, f35 = 0, f36 = 0, f37 = 0, f38 = 0, 
				f39 = 0, f40 = 0, f41 = 0, f42 = 0, f43 = 0, f44 = 0,
				f45 = 0, f46 = 0, f47 = 0, f48 = 0, f49 = 0, f50 = 0,
				f51 = 0, f52 = 0, f53 = 0;

		float f54 = 0, f55 = 0, f56 = 0, f57 = 0, f58 = 0, f59 = 0, f60 = 0
				, f61 = 0, f62 = 0;
		
		float f63 = 0;
		
		float f64 = 0, f65 = 0, f66 = 0, f67 = 0, f68 = 0, f69 = 0, f70 = 0;
		float f71 = 0, f72 = 0, f73 = 0, f74 = 0, f75 = 0, f76 = 0, f77 = 0;
		float f78 = 0, f79 = 0, f80 = 0, f81 = 0, f82 = 0, f83 = 0, f84 = 0;
		float f85 = 0, f86 = 0, f87 = 0;
		float f88 = 0, f89 = 0, f90 = 0, f91 = 0, f92 = 0, f93 = 0, f94 = 0;
		float f95 = 0, f96 = 0, f97 = 0, f98 = 0, f99 = 0, f100 = 0;
		float f101 = 0, f102 = 0, f103 = 0, f104 = 0, f105 = 0, f106 = 0, f107 = 0;
		float f108 = 0;
		
		float f109 = 0, f110 = 0, f111 = 0, f112 = 0, f113 = 0, f114 = 0;
		
		DocumentPreprocessor dp = new DocumentPreprocessor(filePath);
		
		VocabularyRichness vr = new VocabularyRichness();

		HashMap<String, Integer> word_frequency = new HashMap<>();
		float sentence_count = 0;
		float word_count = 0;
		float syllable_count = 0;
		for (List<HasWord> sentence : dp) {
			sentence_count++;
			float charCount = sentence.toString().length();
			
			f5 += sentence.toString().split("\\s+").length - 1;

			for (HasWord word: sentence) {
				
				word_count++;
				
				syllable_count += vr.countSyllables(word.toString().toLowerCase());
				
				if (word_frequency.containsKey(word.toString().toLowerCase())) {
					word_frequency.put(word.toString().toLowerCase(), word_frequency.get(word.toString().toLowerCase())+1);
				} else {
					word_frequency.put(word.toString().toLowerCase(), 1);
				}
				f54+=1;
				if (word.toString().length() <= 4) {
					f55+=1;
				}
				f56+=word.toString().length()/charCount;
				f1+=word.toString().length();
				if (word.toString().matches("[A-Z|a-z]*")) {
					f2+=word.toString().length();
				}
				f57+=word.toString().length();
				char[] arr = word.toString().toCharArray();
				for (int i=0;i<arr.length;i++) {
					if (Character.toString(arr[i]).matches("[A-Z]")) {
						f3+=1;
					} else if (Character.toString(arr[i]).matches("[0-9]")) {
						f4+=1;
					} 

					if (matchChar(arr[i], "[Aa]")) {
						f7+=1;
					}
					if (matchChar(arr[i], "[Bb]")) {
						f8+=1;
					}
					if (matchChar(arr[i], "[Cc]")) {
						f9+=1;
					}
					if (matchChar(arr[i], "[Dd]")) {
						f10+=1;
					}
					if (matchChar(arr[i], "[Ee]")) {
						f11+=1;
					}
					if (matchChar(arr[i], "[Ff]")) {
						f12+=1;
					}
					if (matchChar(arr[i], "[Gg]")) {
						f13+=1;
					}
					if (matchChar(arr[i], "[Hh]")) {
						f14+=1;
					}
					if (matchChar(arr[i], "[Ii]")) {
						f15+=1;
					}
					if (matchChar(arr[i], "[Jj]")) {
						f16+=1;
					}
					if (matchChar(arr[i], "[Kk]")) {
						f17+=1;
					}
					if (matchChar(arr[i], "[Ll]")) {
						f18+=1;
					}
					if (matchChar(arr[i], "[Mm]")) {
						f19+=1;
					}
					if (matchChar(arr[i], "[Nn]")) {
						f20+=1;
					}
					if (matchChar(arr[i], "[Oo]")) {
						f21+=1;
					}
					if (matchChar(arr[i], "[Pp]")) {
						f22+=1;
					}
					if (matchChar(arr[i], "[Qq]")) {
						f23+=1;
					}
					if (matchChar(arr[i], "[Rr]")) {
						f24+=1;
					}
					if (matchChar(arr[i], "[Ss]")) {
						f25+=1;
					}
					if (matchChar(arr[i], "[Tt]")) {
						f26+=1;
					}
					if (matchChar(arr[i], "[Uu]")) {
						f27+=1;
					}
					if (matchChar(arr[i], "[Vv]")) {
						f28+=1;
					}
					if (matchChar(arr[i], "[Ww]")) {
						f29+=1;
					}
					if (matchChar(arr[i], "[Xx]")) {
						f30+=1;
					}
					if (matchChar(arr[i], "[Yy]")) {
						f31+=1;
					}
					if (matchChar(arr[i], "[Zz]")) {
						f32+=1;
					}

					if (matchChar(arr[i], "[~]")) {
						f33+=1;
					}
					if (matchChar(arr[i], "[@]")) {
						f34+=1;
					}
					if (matchChar(arr[i], "[#]")) {
						f35+=1;
					}
					if (matchChar(arr[i], "[$]")) {
						f36+=1;
					}
					if (matchChar(arr[i], "[%]")) {
						f37+=1;
					}
					if (matchChar(arr[i], "[\\^]")) {
						f38+=1;
					}
					if (matchChar(arr[i], "[&]")) {
						f39+=1;
					}
					if (matchChar(arr[i], "[*]")) {
						f40+=1;
					}
					if (matchChar(arr[i], "[-]")) {
						f41+=1;
					}
					if (matchChar(arr[i], "[_]")) {
						f42+=1;
					}
					if (matchChar(arr[i], "[=]")) {
						f43+=1;
					}
					if (matchChar(arr[i], "[+]")) {
						f44+=1;
					}
					if (matchChar(arr[i], "[>]")) {
						f45+=1;
					}
					if (matchChar(arr[i], "[<]")) {
						f46+=1;
					}
					if (matchChar(arr[i], "\\[")) {
						f47+=1;
					}
					if (matchChar(arr[i], "]")) {
						f48+=1;
					}
					if (matchChar(arr[i], "[{]")) {
						f49+=1;
					}
					if (matchChar(arr[i], "[}]")) {
						f50+=1;
					}
					if (matchChar(arr[i], "[/]")) {
						f51+=1;
					}
					if (matchChar(arr[i], "[\\\\]")) {
						f52+=1;
					}
					if (matchChar(arr[i], "[|]")) {
						f53+=1;
					}
				}
			}
		}
	    
		f55 = f55/f54;
		f57 = f56/word_count;
		f56 = f56/f1;
		f58 = f1/sentence_count;
		f59 = word_count/sentence_count;

		f2=f2/f1; f3=f3/f1; f4=f4/f1; f5=f5/f1; f6=f6/f1; f7=f7/f1; f8=f8/f1; f9=f9/f1; f10=f10/f1;
		f11=f11/f1; f12=f12/f1; f13=f13/f1; f14=f14/f1; f15=f15/f1; f16=f16/f1; f17=f17/f1; f18=f18/f1;
		f19=f19/f1; f20=f20/f1; f21=f21/f1; f22=f22/f1; f23=f23/f1; f24=f24/f1; f25=f25/f1; f26=f26/f1;
		f27=f27/f1; f28=f28/f1; f29=f29/f1; f30=f30/f1; f31=f31/f1; f32=f32/f1; f33=f33/f1; f34=f34/f1;
		f35=f35/f1; f36=f36/f1; f37=f37/f1; f38=f38/f1; f39=f39/f1; f40=f40/f1; f41=f41/f1; f42=f42/f1;
		f43=f43/f1; f44=f44/f1; f45=f45/f1; f46=f46/f1; f47=f47/f1; f48=f48/f1; f49=f49/f1; f50=f50/f1;
		f51=f51/f1; f52=f52/f1; f53=f53/f1;

		// Total different words
		f60 = word_frequency.size()/f54;
		//Hapax legomena
		f61 = getFrequencyOfWords(word_frequency).get(1)/f54;
		f62 = getFrequencyOfWords(word_frequency).get(2)/f54;
		
		POSTaggerWrapper ptw = new POSTaggerWrapper();
		HashMap<String, Float> hm = new HashMap<>();
		hm.putAll(ptw.getPOSFrequencyForDocument(filePath));
		
		f64 = hm.get("#")/f54;
		f65 = hm.get("$")/f54;
		f66 = hm.get("\"")/f54;
		f67 = hm.get("\"")/f54;
		f68 = hm.get(",")/f54;
		f69 = hm.get("-LRB-")/f54;
		f70 = hm.get("-RRB-")/f54;
		f71 = hm.get(".")/f54;
		f72 = hm.get(":")/f54;
		f73 = hm.get("CC")/f54;
		f74 = hm.get("CD")/f54;
		f75 = hm.get("DT")/f54;
		f76 = hm.get("EX")/f54;
		f77 = hm.get("FW")/f54;
		f78 = hm.get("IN")/f54;
		f79 = hm.get("JJ")/f54;
		f80 = hm.get("JJR")/f54;
		f81 = hm.get("JJS")/f54;
		f82 = hm.get("LS")/f54;
		f83 = hm.get("MD")/f54;
		f84 = hm.get("NN")/f54;
		f85 = hm.get("NNP")/f54;
		f86 = hm.get("NNPS")/f54;
		f87 = hm.get("NNS")/f54;
		f88 = hm.get("PDT")/f54;
		f89 = hm.get("POS")/f54;
		f90 = hm.get("PRP")/f54;
		f91 = hm.get("PRP$")/f54;
		f92 = hm.get("RB")/f54;
		f93 = hm.get("RBR")/f54;
		f94 = hm.get("RBS")/f54;
		f95 = hm.get("RP")/f54;
		f96 = hm.get("SYM")/f54;
		f97 = hm.get("TO")/f54;
		f98 = hm.get("UH")/f54;
		f99 = hm.get("VB")/f54;
		f100 = hm.get("VBD")/f54;
		f101 = hm.get("VBG")/f54;
		f102 = hm.get("VBN")/f54;
		f103 = hm.get("VBP")/f54;
		f104 = hm.get("VBZ")/f54;
		f105 = hm.get("WDT")/f54;
		f106 = hm.get("WP")/f54;
		f107 = hm.get("WP$")/f54;
		f108 = hm.get("WRB")/f54;
		
		// Vocabulary richness
		f109 = 206.835f - 1.015f * (word_count / sentence_count) - 84.6f * (syllable_count / word_count);
		f110 = vr.getSimpsons(word_frequency, word_count);
		f111 = vr.getYules(word_frequency, word_count);
		f112 = (float) ((f62 / word_frequency.size())*((float)Math.pow(10,3)));
		f113 = (float) Math.pow(word_count, Math.pow(word_frequency.size(), -0.17));
		f114 = 100 * (float) Math.log(word_count) / (1 - (f61 / word_frequency.size()));

		// Sentence Similarity
		SentenceSimilarity ss = new SentenceSimilarity();
		f63 = ss.getSimilarityScoreForDocument(filePath, (int)sentence_count);
		
		featureSet.add(f1);featureSet.add(f2);featureSet.add(f3);featureSet.add(f4);featureSet.add(f5);
		featureSet.add(f6);featureSet.add(f7);featureSet.add(f8);featureSet.add(f9);featureSet.add(f10);
		featureSet.add(f11);featureSet.add(f12);featureSet.add(f13);featureSet.add(f14);featureSet.add(f15);
		featureSet.add(f16);featureSet.add(f17);featureSet.add(f18);featureSet.add(f19);featureSet.add(f20);
		featureSet.add(f21);featureSet.add(f22);featureSet.add(f23);featureSet.add(f24);featureSet.add(f25);
		featureSet.add(f26);featureSet.add(f27);featureSet.add(f28);featureSet.add(f29);featureSet.add(f30);
		featureSet.add(f31);featureSet.add(f32);featureSet.add(f33);featureSet.add(f34);featureSet.add(f35);
		featureSet.add(f36);featureSet.add(f37);featureSet.add(f38);featureSet.add(f39);featureSet.add(f40);
		featureSet.add(f41);featureSet.add(f42);featureSet.add(f43);featureSet.add(f44);featureSet.add(f45);
		featureSet.add(f46);featureSet.add(f47);featureSet.add(f48);featureSet.add(f49);featureSet.add(f50);
		featureSet.add(f51);featureSet.add(f52);featureSet.add(f53);

		featureSet.add(f54);featureSet.add(f55);featureSet.add(f56);featureSet.add(f57);featureSet.add(f58);
		featureSet.add(f59);featureSet.add(f60);featureSet.add(f61);featureSet.add(f62);

		featureSet.add(f63);
		
		featureSet.add(f64);featureSet.add(f65);featureSet.add(f66);featureSet.add(f67);featureSet.add(f68);
		featureSet.add(f69);featureSet.add(f70);
		featureSet.add(f71);featureSet.add(f72);featureSet.add(f73);featureSet.add(f74);featureSet.add(f75);
		featureSet.add(f76);featureSet.add(f77);featureSet.add(f78);featureSet.add(f79);featureSet.add(f80);
		featureSet.add(f81);featureSet.add(f82);featureSet.add(f83);featureSet.add(f84);featureSet.add(f85);
		featureSet.add(f86);featureSet.add(f87);featureSet.add(f88);featureSet.add(f89);featureSet.add(f90);
		featureSet.add(f91);featureSet.add(f92);featureSet.add(f93);featureSet.add(f94);featureSet.add(f95);
		featureSet.add(f96);featureSet.add(f97);featureSet.add(f98);featureSet.add(f99);featureSet.add(f100);
		featureSet.add(f101);featureSet.add(f102);featureSet.add(f103);featureSet.add(f104);featureSet.add(f105);
		featureSet.add(f106);featureSet.add(f107);featureSet.add(f108);
		
		featureSet.add(f109);featureSet.add(f110);featureSet.add(f111);featureSet.add(f112);featureSet.add(f113);
		featureSet.add(f114);
		return featureSet;
	}

	private Boolean matchChar(char a, String pattern) {
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(Character.toString(a));
		if (m.matches()) {
			return true;
		}
		return false;
	}

	private HashMap<Integer, Float> getFrequencyOfWords(HashMap<String, Integer> hm) {
		HashMap<Integer, Float> frequency = new HashMap<>();
		float countOnce = 0;
		float countTwice = 0;
		@SuppressWarnings("rawtypes")
		Iterator itx = hm.entrySet().iterator();
		while (itx.hasNext()) {
			@SuppressWarnings("unchecked")
			Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>) itx.next();
			if (pair.getValue() == 1) {
				countOnce++;
			} else if (pair.getValue() == 2) {
				countTwice++;
			}
		}
		frequency.put(1, countOnce);
		frequency.put(2, countTwice);
		return frequency;
	}
}
