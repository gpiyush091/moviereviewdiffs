package featureSet;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class VocabularyRichness {
	
	private boolean isVowel(char c) {
		return (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y');
	}

	public int countSyllables(String word)
    {
        int numVowels = 0;
        boolean vowelGroup = false;
        // Count groups of vowels (roughly equal to syllable count)
        for (int i = 0; i < word.length(); i++)
        {
            if (isVowel(word.charAt(i)))
            {
            	if (!vowelGroup) {
                    numVowels++;
                    vowelGroup = true;
            	}
            }
            else {
            	vowelGroup = false;
            }
        }
        // Don't count silent es at end of word
        if (word.length() > 2 && 
            word.substring(word.length() - 2).equals("es")) {
            numVowels--;
        }
        // Don't count silent e at end of word
        else if (word.length() > 1 &&
            word.charAt(word.length()-1) == 'e') {
            numVowels--;
        }
        
        // All words have at least one syllable
        if (numVowels < 1)
        	numVowels = 1;

        return numVowels;
    }

	public float getSimpsons(HashMap<String, Integer> hm, float word_count) {
		float simpsons_measure = 0;
		@SuppressWarnings("rawtypes")
		Iterator itx = hm.entrySet().iterator();
		while (itx.hasNext()) {
			@SuppressWarnings("unchecked")
			Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>) itx.next();
			simpsons_measure += pair.getValue() * (pair.getValue() - 1);
		}
		
		simpsons_measure = simpsons_measure / (word_count * (word_count - 1));
		return simpsons_measure;
	}
	
	public float getYules(HashMap<String, Integer> hm, float word_count) {
		float yules_measure = 0;
		@SuppressWarnings("rawtypes")
		Iterator itx = hm.entrySet().iterator();
		while (itx.hasNext()) {
			@SuppressWarnings("unchecked")
			Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>) itx.next();
			yules_measure += pair.getValue() * pair.getValue();
		}
		
		yules_measure = 10000 * (yules_measure - word_count) / (word_count * word_count);
		return yules_measure;
	}

}
