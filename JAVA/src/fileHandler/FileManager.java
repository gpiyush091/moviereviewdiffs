package fileHandler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import datastructure.Reviewfeatures;

public class FileManager {
	private final String searchStringFilePath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\intermediate-critic-list.html";
	private final String reviewerStartID = "<h2>";
	private final String reviewerEndID = "</h2>";
	private final String reviewerDataStartID = "<p class=\"data\">";
	private final String reviewerDataEndID = "</p>";
	
	private final String fullReviewersDirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\hunger_games_2013";
	private final String fullReviewersTxtDirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\hunger_games_2013_rt";
	
	public ArrayList<String> getFileList(String dirPath) {
		ArrayList<String> filesList = new ArrayList<>();
		File folder = new File(dirPath);
		if (folder.isDirectory()) {
			
			File[] files = folder.listFiles(new FilenameFilter() {

				@Override
				public boolean accept(File folder, String fileName) {
					return true;
				}
			});
			
			for(int i=0;i<files.length;i++) {
				filesList.add(files[i].getAbsolutePath());
			}
		}
		return filesList;
	}
	
	/*
	 * Used to extract reviews content from HTML without knowing the tag element. Search based on keywords in Rotten Tomatoes!
	 * */
	public void extractReviewsFromFiles() {
		File file = new File(searchStringFilePath);
		BufferedReader br = null;
		
		String str = "";
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			String reviewer = null;
			String reviewer_abstract = null;
			
			while ((str = br.readLine())!=null){
				if(str.contains(reviewerStartID)) {
					reviewer = str.substring(str.indexOf(reviewerStartID)+reviewerStartID.length(), str.indexOf(reviewerEndID));
				}
				if (str.contains(reviewerDataStartID)) {
					reviewer_abstract = str.substring(str.indexOf(reviewerDataStartID)+2+reviewerDataStartID.length(), str.indexOf(reviewerDataEndID)-2);
				}
				
				if (reviewer != null && reviewer_abstract != null) {
					searchForFullReview(reviewer, reviewer_abstract);
					reviewer = null;
					reviewer_abstract = null;
				}
			}
			

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br!=null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private static String reviewerGlobal = ""; 
	
	public void searchForFullReview(String reviewer, String rev_abstract) {
		reviewerGlobal = reviewer;
		File folder = new File(fullReviewersDirPath);
		if (folder.isDirectory()) {
			
			File[] files = folder.listFiles(new FilenameFilter() {

				@Override
				public boolean accept(File folder, String fileName) {
					return fileName.endsWith(reviewerGlobal+".html");
				}
			});
			
			fetchAndParseHTML(files[0], rev_abstract, reviewer);
		}
	}
	
	private void fetchAndParseHTML(File file, String rev_abstract, String reviewer) {
		if (reviewer.equals("Ty Burr")) { // exceptional case
			return;
		}
		Document doc;
		
		System.out.println("in here: "+rev_abstract);
		try {
			doc = Jsoup.parse(file, "UTF-8");
			//System.out.println(doc);
			Elements divElements = doc.getElementsByTag("div");
			
			System.out.println();
			
			for (Element ele : divElements) {
				Elements pEles = ele.getElementsByTag("p");
				if (pEles.size()>0) {
					Boolean flag = false;
					Element x = null;
					for (Element pEle: pEles) {
						String str = new String(pEle.toString());
						// System.out.println(str);
						if (str.contains("&quot;")) {
							str = str.replaceAll("&quot;", "\"");
						}
						if (str.contains("&rdquo;")) {
							str = str.replaceAll("&rdquo;", "\"");
						}
						if (str.contains("&ldquo;")) {
							str = str.replaceAll("&ldquo;", "\"");
						}
						if (str.contains("&mdash;")) {
							str = str.replaceAll("&mdash;", "-");
						}
						if (str.contains("&quot;")) {
							str = str.replaceAll("&rsquo;", "'");
						}
						if (str.contains("&#8220;")) {
							str = str.replaceAll("&#8220;", "\"");
						}
						if (str.contains("&#8221;")) {
							str = str.replaceAll("&#8221;", "\"");
						}
						if (str.contains("&#8217;")) {
							str = str.replaceAll("&#8217;", "'");
						}
						
						if (str.contains(rev_abstract)) {
							flag = true;
							x = pEle;
							break;
						}
					}
					if (flag) {
						Elements articleElements = x.parent().getElementsByTag("p");
						StringBuilder article =  new StringBuilder();
						for (Element aEle:articleElements) {
							if (aEle.toString().contains("<p class") || aEle.toString().contains("<p><")) {
								continue;
							} else {
								article.append(aEle.text()+" ");
							}
						}
						if (article.toString().length()>0) {
							String articleStr = article.toString();
							if (articleStr.contains("&quot;")) {
								articleStr = articleStr.replaceAll("&quot;", "\"");
							}
							if (articleStr.contains("&rdquo;")) {
								articleStr = articleStr.replaceAll("&rdquo;", "\"");
							}
							if (articleStr.contains("&ldquo;")) {
								articleStr = articleStr.replaceAll("&ldquo;", "\"");
							}
							if (articleStr.contains("&mdash;")) {
								articleStr = articleStr.replaceAll("&mdash;", "-");
							}
							if (articleStr.contains("&quot;")) {
								articleStr = articleStr.replaceAll("&rsquo;", "'");
							}
							if (articleStr.contains("&#8220;")) {
								articleStr = articleStr.replaceAll("&#8220;", "\"");
							}
							if (articleStr.contains("&#8221;")) {
								articleStr = articleStr.replaceAll("&#8221;", "\"");
							}
							if (articleStr.contains("&#8217;")) {
								articleStr = articleStr.replaceAll("&#8217;", "'");
							}
							
							// Write to file
							writeToFile(reviewer, articleStr);
							
							System.out.println(reviewer+" : "+articleStr);
							break;
						}
					} else {
						System.out.println(reviewer+" "+rev_abstract);
						System.out.println("SOMETHING IS WRONG !!!");
					}
				}
			}
			System.out.println();
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeFeaturesToCSVFile(String fileName, ArrayList<Reviewfeatures> allFileFeatureSet) {
		File newFile = new File(fileName);
		System.out.println("Create CSV File: "+fileName);
		if (!newFile.exists()) {
			try {
				newFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newFile), "UTF-8"));
			// For each document
			out.append("f1"); out.append(",");
			out.append("f2"); out.append(",");
			out.append("f3"); out.append(",");
			out.append("f4"); out.append(",");
			out.append("f5"); out.append(",");
			out.append("f6"); out.append(",");
			out.append("f7"); out.append(",");
			out.append("f8"); out.append(",");
			out.append("f9"); out.append(",");
			out.append("f10"); out.append(",");

			out.append("f11"); out.append(",");
			out.append("f12"); out.append(",");
			out.append("f13"); out.append(",");
			out.append("f14"); out.append(",");
			out.append("f15"); out.append(",");
			out.append("f16"); out.append(",");
			out.append("f17"); out.append(",");
			out.append("f18"); out.append(",");
			out.append("f19"); out.append(",");
			out.append("f20"); out.append(",");

			out.append("f21"); out.append(",");
			out.append("f22"); out.append(",");
			out.append("f23"); out.append(",");
			out.append("f24"); out.append(",");
			out.append("f25"); out.append(",");
			out.append("f26"); out.append(",");
			out.append("f27"); out.append(",");
			out.append("f28"); out.append(",");
			out.append("f29"); out.append(",");
			out.append("f30"); out.append(",");

			out.append("f31"); out.append(",");
			out.append("f32"); out.append(",");
			out.append("f33"); out.append(",");
			out.append("f34"); out.append(",");
			out.append("f35"); out.append(",");
			out.append("f36"); out.append(",");
			out.append("f37"); out.append(",");
			out.append("f38"); out.append(",");
			out.append("f39"); out.append(",");
			out.append("f40"); out.append(",");

			out.append("f41"); out.append(",");
			out.append("f42"); out.append(",");
			out.append("f43"); out.append(",");
			out.append("f44"); out.append(",");
			out.append("f45"); out.append(",");
			out.append("f46"); out.append(",");
			out.append("f47"); out.append(",");
			out.append("f48"); out.append(",");
			out.append("f49"); out.append(",");
			out.append("f50"); out.append(",");

			out.append("f51"); out.append(",");
			out.append("f52"); out.append(",");
			out.append("f53"); out.append(",");
			out.append("f54"); out.append(",");
			out.append("f55"); out.append(",");
			out.append("f56"); out.append(",");
			out.append("f57"); out.append(",");
			out.append("f58"); out.append(",");
			out.append("f59"); out.append(",");
			out.append("f60"); out.append(",");

			out.append("f61"); out.append(",");
			out.append("f62"); out.append(",");
			out.append("f63"); out.append(",");
			out.append("f64"); out.append(",");
			out.append("f65"); out.append(",");
			out.append("f66"); out.append(",");
			out.append("f67"); out.append(",");
			out.append("f68"); out.append(",");
			out.append("f69"); out.append(",");
			out.append("f70"); out.append(",");

			out.append("f71"); out.append(",");
			out.append("f72"); out.append(",");
			out.append("f73"); out.append(",");
			out.append("f74"); out.append(",");
			out.append("f75"); out.append(",");
			out.append("f76"); out.append(",");
			out.append("f77"); out.append(",");
			out.append("f78"); out.append(",");
			out.append("f79"); out.append(",");
			out.append("f80"); out.append(",");

			out.append("f81"); out.append(",");
			out.append("f82"); out.append(",");
			out.append("f83"); out.append(",");
			out.append("f84"); out.append(",");
			out.append("f85"); out.append(",");
			out.append("f86"); out.append(",");
			out.append("f87"); out.append(",");
			out.append("f88"); out.append(",");
			out.append("f89"); out.append(",");
			out.append("f90"); out.append(",");

			out.append("f91"); out.append(",");
			out.append("f92"); out.append(",");
			out.append("f93"); out.append(",");
			out.append("f94"); out.append(",");
			out.append("f95"); out.append(",");
			out.append("f96"); out.append(",");
			out.append("f97"); out.append(",");
			out.append("f98"); out.append(",");
			out.append("f99"); out.append(",");
			out.append("f100"); out.append(",");

			out.append("f101"); out.append(",");
			out.append("f102"); out.append(",");
			out.append("f103"); out.append(",");
			out.append("f104"); out.append(",");
			out.append("f105"); out.append(",");
			out.append("f106"); out.append(",");
			out.append("f107"); out.append(",");
			out.append("f108"); out.append(",");
			out.append("f109"); out.append(",");
			out.append("f110"); out.append(",");

			out.append("f111"); out.append(",");
			out.append("f112"); out.append(",");
			out.append("f113"); out.append(",");
			out.append("f114");
			
			out.append("\n");
			
			for (int i=0;i<allFileFeatureSet.size();i++) {
				// Get every feature
				out.append(allFileFeatureSet.get(i).featureSet.get(0).toString());
				for (int j=1;j<allFileFeatureSet.get(i).featureSet.size();j++) {
					out.append(",");
					out.append(allFileFeatureSet.get(i).featureSet.get(j).toString());
				}
				out.append("\n");
			}
			out.flush();
			out.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeFeaturesToFile(String fileName, ArrayList<Reviewfeatures> featureSet) {
		File newFile = new File(fileName);

		if (!newFile.exists()) {
			try {
				newFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newFile), "UTF-8"));
			// For each document
			for (int i=0;i<featureSet.size();i++) {
				// Get every feature
				if (featureSet.get(i).category) {
					out.append("+1"+" ");
				} else {
					out.append("-1"+" ");
				}
				for (int j=0;j<featureSet.get(i).featureSet.size();j++) {
					out.append(j+1+":"+featureSet.get(i).featureSet.get(j).toString()+" ");
				}
				out.append("\n");
			}
			out.flush();
			out.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeToFile(String reviewer, String articleStr) {
		File newFile = new File(fullReviewersTxtDirPath+"\\"+reviewer+".txt");

		if (!newFile.exists()) {
			try {
				newFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newFile), "UTF-8"));
			out.write(articleStr);
			out.flush();
			out.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
