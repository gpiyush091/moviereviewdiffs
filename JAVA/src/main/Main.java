   package main;

import java.util.ArrayList;

import datastructure.Reviewfeatures;
import featureSet.Features;
import fileHandler.FileManager;

public class Main {

	
	/*
	 * true for SVM format file
	 * false for k means format file
	 * */
	private void process(String inputDir, String outputFile, Boolean isSVM) {
		FileManager fm = new FileManager();
		ArrayList<String> filesList = fm.getFileList(inputDir);
		Features f = new Features();
		ArrayList<Reviewfeatures> allFileFeatureSet = new ArrayList<>();
		
		for (int i=0; i<filesList.size();i++) {
			Reviewfeatures rf = new Reviewfeatures();
			rf.featureSet.addAll(f.getFeatures(filesList.get(i)));
			rf.category = true;
			allFileFeatureSet.add(rf);
		}
		if (isSVM) {
			fm.writeFeaturesToFile(outputFile, allFileFeatureSet);
		} else {
			fm.writeFeaturesToCSVFile(outputFile, allFileFeatureSet);
		}
	}
	
	public static void main(String[] args) {
		System.out.println("Main Class");
		Main m = new Main();
		
		String dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\RT_Data\\Hunger_Games";
		String fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\CSV\\RT_Data\\Hunger_Games_wo_wiki.csv";
		m.process(dirPath, fileName, false);
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\AmazonReviews\\Hunger_Games";
		fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\CSV\\AmazonReviews\\Hunger_Games_wo_wiki.csv";
		m.process(dirPath, fileName, false);
		
		
		
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\RT_Data\\Blackfish";
		fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\CSV\\RT_Data\\Blackfish_wo_wiki.csv";
		m.process(dirPath, fileName, false);
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\AmazonReviews\\Blackfish";
		fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\CSV\\AmazonReviews\\Blackfish_wo_wiki.csv";
		m.process(dirPath, fileName, false);
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\RT_Data\\Inception";
		fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\CSV\\RT_Data\\Inception_wo_wiki.csv";
		m.process(dirPath, fileName, false);
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\AmazonReviews\\Inception";
		fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\CSV\\AmazonReviews\\Inception_wo_wiki.csv";
		m.process(dirPath, fileName, false);
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\RT_Data\\Pans_Labyrinth";
		fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\CSV\\RT_Data\\Pans_Labyrinth_wo_wiki.csv";
		m.process(dirPath, fileName, false);
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\AmazonReviews\\Pans_Labyrinth";
		fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\CSV\\AmazonReviews\\Pans_Labyrinth_wo_wiki.csv";
		m.process(dirPath, fileName, false);
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\RT_Data\\The_Artist";
		fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\CSV\\RT_Data\\The_Artist_wo_wiki.csv";
		m.process(dirPath, fileName, false);
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\AmazonReviews\\The_Artist";
		fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\CSV\\AmazonReviews\\The_Artist_wo_wiki.csv";
		m.process(dirPath, fileName, false);
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\RT_Data\\Up";
		fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\CSV\\RT_Data\\Up_wo_wiki.csv";
		m.process(dirPath, fileName, false);
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\AmazonReviews\\Up";
		fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\Dataset\\CSV\\AmazonReviews\\Up_wo_wiki.csv";
		m.process(dirPath, fileName, false);
		
		System.out.println("Main Ends");
		
		// Signals End of code
		while (true)
		{
		  java.awt.Toolkit.getDefaultToolkit().beep();
		}
		
		//C:\Users\Gaurav\Music\iTunes\iTunes Media\Music\Imagine Dragons\Night Visions (Deluxe Version)
		
		
		//============================================
		// TODO Auto-generated method stub
		// fm.extractReviewsFromFiles();
		
		// TRAIN
		/*
		String dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\train\\good"; // change here
		ArrayList<String> filesList = fm.getFileList(dirPath);
		Features f = new Features();
		ArrayList<Reviewfeatures> allFeaturesTrainAllFiles = new ArrayList<>();
		/*
		for (int i=0; i<filesList.size();i++) {
			Reviewfeatures rf = new Reviewfeatures();
			rf.featureSet.addAll(f.getFeatures(filesList.get(i)));
			rf.category = true;
			allFeaturesTrainAllFiles.add(rf);
		}
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\train\\bad";
		filesList.clear();
		filesList.addAll(fm.getFileList(dirPath));
		for (int i=0; i<filesList.size();i++) {
			Reviewfeatures rf = new Reviewfeatures();
			rf.featureSet.addAll(f.getFeatures(filesList.get(i)));
			rf.category = false;
			allFeaturesTrainAllFiles.add(rf);
		}
		
		String fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\TrainAllFeaturesAllFiles.dat";
		fm.writeFeaturesToFile(fileName, allFeaturesTrainAllFiles);
		
		
		// TEST
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\test\\good"; // change here
		filesList.clear();
		filesList.addAll(fm.getFileList(dirPath));
		ArrayList<Reviewfeatures> allFeaturesTestAllFiles = new ArrayList<>();
		for (int i=0; i<filesList.size();i++) {
			Reviewfeatures rf = new Reviewfeatures();
			rf.featureSet.addAll(f.getFeatures(filesList.get(i)));
			rf.category = true;
			allFeaturesTestAllFiles.add(rf);
		}
		
		dirPath = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\test\\bad";
		filesList.clear();
		filesList.addAll(fm.getFileList(dirPath));
		for (int i=0; i<filesList.size();i++) {
			Reviewfeatures rf = new Reviewfeatures();
			rf.featureSet.addAll(f.getFeatures(filesList.get(i)));
			rf.category = false;
			allFeaturesTestAllFiles.add(rf);
		}
		
		fileName = "F:\\remote\\Spring'14\\NLP\\NLP_Project\\TestAllFeaturesAllFiles.dat";
		fm.writeFeaturesToFile(fileName, allFeaturesTestAllFiles);
		*/
		//System.out.println("Main Ends");
	}
}
