require 'open-uri'
require 'rubygems'
require 'mechanize'
require 'pp'
require 'nokogiri'

# name. rating, review, link_to_review
#tmp_file = 'intermediate-critic-list.html'
agent = Mechanize.new
agent.get("http://www.rottentomatoes.com/m/the_hunger_games_catching_fire/reviews/#") do |page|
	page = agent.click(page.link_with(:text => 'Top Critics'))
	#get only the review list !
	#file = open(tmp_file,'w')
	doc = Nokogiri::HTML(page.body)
	reviewsData = doc.xpath('//div[@id="reviews"]')
	perReviewer = reviewsData.xpath('//div[@class="criticinfo"]/strong/a')
	i=0
	j=0
	text = ""
	while i<perReviewer.length
		reviewerName = reviewsData.xpath('//div[@class="criticinfo"]/strong/a')[i].text.strip
		text = text+"<h2>"+reviewerName+"</h2>\n"
		
		reviewLink = reviewsData.xpath('//p[@class="small subtle"]/a')[i].attributes['href']
		text = text+"<p class=\"link\"><a href="+reviewLink+">"+reviewLink+"</a></p>\n"
		reviewData = reviewsData.xpath('//div[@class="reviewsnippet"]/p')[i*2].text
		
		# assuming every review is atleast 7 characters long.. reasonable assumption
		testData = reviewData[2..-5]
		text = text+"<p class=\"data\">"+reviewData+"</p>\n"
		reviewRating = reviewsData.xpath('//p[@class="small subtle"]')[i].text.strip
		reviewRating = reviewRating.gsub(/Full Review



















	
	

 
					
					\| Original Score: /,"")
		text = text+"<p class=\"rating\">"+reviewRating+"</p>\n"
		print "\n"
		print reviewerName
		print "\n"
		print reviewLink
		print "\n"
		# parse each author and take it to the full review page and crawl from there !
		agentCustom = Mechanize.new
		
		uri = URI.parse(reviewLink)
		
		result = Net::HTTP.start(uri.host, uri.port) { |http| http.get(uri.path) }
		
		if result.code == "200"
			agentCustom.get(reviewLink) do |reviewPage|
				print "\n"
				print reviewerName
				print "\n"
				print reviewLink
				print "\n"		
				file_t = open("hunger_games_2013/"+reviewerName+".html", "w")
				rev = Nokogiri::HTML(reviewPage.body)#open(reviewLink))
				#ic_ignore = Iconv.new('US-ASCII//IGNORE//TRANSIT', 'UTF-8')
				#ic_ignore.iconv(rev)
				tmpText = rev.inner_html
				print rev.encoding
				#~ if rev.encoding == "iso-8859-1"
					#~ tmpText = tmpText.gsub(/&#8216;/,"'")
					#~ tmpText = tmpText.gsub(/&#8217;/,"'")
					#~ tmpText = tmpText.gsub(/&#8220;/,"\"")
					#~ tmpText = tmpText.gsub(/&#8221;�/,"\"")
					#~ tmpText = tmpText.gsub(/&#8201;/," ")
					#~ tmpText = tmpText.gsub(/&#8212;/,"-")
					#~ tmpText = tmpText.gsub(/\t/,"")
				#~ else if rev.encoding == "utf-8"
				if rev.encoding == "UTF-8"
					tmpText = tmpText.gsub(/’/,"'")
					tmpText = tmpText.gsub(/“/,"\"")
					tmpText = tmpText.gsub(/”/,"\"")
					tmpText = tmpText.gsub(/\t/,"")
				end
				
				if rev.encoding == "utf-8"
					tmpText = tmpText.gsub(/’/,"'")
					tmpText = tmpText.gsub(/“/,"\"")
					tmpText = tmpText.gsub(/”/,"\"")
					tmpText = tmpText.gsub(/\t/,"")
				end
				
				print "\n"
				print "\n"
				print "\n"
				
				file_t.write(tmpText)
				file_t.close
			end
			j=j+1
		end
		i=i+1
	end
	print j
	#file.write(text)
	#file.close
end