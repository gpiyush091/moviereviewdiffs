"""
Parses Hunger Games data into separate files for each review
"""

import csv
import os
import sys
from HTMLParser import HTMLParser

def get_reviews_for_product(product_id):
    """
    Get all the reviews for a particular product. Keeps only reviews with a helpfulness rating
    above 70% and a star value above 3. Writes each review to a file and all metadata to 
    another file in the META folder.
    Metadata contains:
    unique review id, date of review, review's rating, number of "yes" helpfulness votes, 
    total helpfulness votes, author's id, and review title
    """

    parser = HTMLParser()
    with open("CSVs/{}.csv".format(product_id)) as f:
        with open("AmazonReviews/META/{}".format(product_id), 'w') as info_file:
            csv_reader = csv.reader(f)
            for row in csv_reader:
                # Extract review data from CSV file for the product
                count = row[0]
                rating = row[3]
                yes_votes = row[4]
                total_votes = row[5]
                date = row[6]
                author_id = row[7]
                text = row[-1]
                
                # Keep only reviews with a star value above 3
                if float(rating) > 3:
                    # Create a unique id for the review based on its product id and the review number
                    review_id = "{}_{}".format(product_id, count)

                    # Write metadata to file
                    info_file.write("{}\t{}\t{}\t{}\t{}\t{}\n".format(review_id, date, rating, yes_votes, total_votes, author_id))
                    
                    # Create a folder for this product's reviews if it doesn't already exist
                    path = "AmazonReviews/{}".format(product_id)
                    if not os.path.exists(path):
                        os.mkdir(path)

                    # Write review to file
                    with open(path + "/{}".format(review_id), 'w') as review_file:
                        try:
                            unescaped = parser.unescape(text)
                        except:
                            text = text.decode("ascii", "ignore")
                            unescaped = parser.unescape(text)
                        try:
                            t = unescaped.encode("utf8")
                        except:
                            t = text
                        review_file.write(t)

def get_reviews(id_file):
    with open(id_file) as f:
        for line in f:
            title, product_id = line.strip().split("\t")
            print product_id
            get_reviews_for_product(product_id)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        id_file = sys.argv[1]
        get_reviews(id_file)
