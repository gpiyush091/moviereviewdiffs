Fantasy films don't often have the ambition to be historic allegories, but Pan's Labyrinth does. A violent fantasy set during the Spanish Civil War, this magical film from Guillermo del Toro (Mimic) manages that intellectual high-mindedness, even as it resonates on a primal, mythic level.

A Mexican writer-director, del Toro knows his Spanish history. But he also knows his Joseph Campbell and his Vladimir Propp. This story of a young girl who escapes into a dangerous world of fairies and beasts has Campbell's Hero With a Thousand Faces tests, magic talismans, moments of failure and heartfelt triumphs. It is a film cooked up with many a classic folktale's required ingredients, as described, long ago, by Propp, a story analyst from Russia.



Pan's Labyrinth is a stunning blend of the magically surreal with the graphically real. It contrasts the real horrors of the Spanish Civil War -- torture, atrocities, murder and combat -- with the fantasy terrors and rewards of an enchanted labyrinth.

That's what impressionable Ofelia (Ivana Baquero), who is about 10, finds upon arriving at the new posting of her bloody-minded stepfather (Sergi Lopez), a sadistic captain in the fascist army sent to put down the rebels in this wooded corner of Spain. Ofelia reads her fairy tales and tries to comfort her weak, pregnant mother (Ariadna Gil). But on arriving at the old mill where they are now to live, she spies fairies. She soon meets Pan, who charms her and tells her she is the queen whom they have been awaiting in his enchanted, ancient labyrinth.

Ofelia, to take the throne, has to prove herself. Every night, she is given a task. Some quests she passes. Some she fails.

The magic spills over into her daily life, where she must protect her mother and unborn brother from the monster Mom has married. The Captain personally extracts confessions from suspected partisans. He personally executes them. He seems to enjoy it. Ofelia tries to use the magic she has discovered to protect herself, her mother and her sibling-to-be.

Del Toro has a zest for fantasy and unflinching horror (Hellboy, The Devil's Backbone). Here, for all the faceless ghouls and gargantuan toads that Ofelia faces at night, the reality of her days that he conjures up is much worse. Life in a combat zone is not pretty.

Mercedes (Maribel Verdu), the housekeeper, struggles to hide her partisan loyalties from the killer captain, and to save Ofelia from her father, made terrifyingly real and cruel by Lopez. Lopez makes the man so evil he would twirl his moustache, if he had one.

It's a story of stark simplicity. Every third-act climax is foreshadowed by a first-act explanation. Why are this mother and daughter here?

"Because a son should be born where his father is," the captain spits.

Why don't the rebels and their sympathizers give up? All the captain wants is loyalty, a willingness to follow orders, even from his doctor.

"But captain, to obey for obey's sake . . . that's something only people like you do."

Lyrically written, affectingly acted and poetically filmed, Pan's Labyrinth, in Spanish with English subtitles, is brilliant enough to lift an entire genre, simply by its presence. It is one of the most acclaimed films of 2006, and justly so.

And Baquero, as the heroine, written into a tale that dates from when time began, touches us with her humanity, her pluck and her hope that somewhere deep down, we all have what it takes to navigate this labyrinth the world hurls us into.