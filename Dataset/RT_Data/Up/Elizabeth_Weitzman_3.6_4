Has there ever been a company with Pixar's unqualified success rate?
Amazingly enough, their secret seems to be as straightforward as the projects are complex: they simply refuse to underestimate their audience.
Where most animated films pander to children and wink at adults, the Pixar geniuses assume we're all equals, and equally deserving of the best they can create.
While their latest achievement can't quite one-up "WALL-E," it offers soaring highs that are bound to enchant viewers of any age.
From title to trailers, "Up" appears to be light as air. But there's an unapologetically dark streak to this story, which begins when two young adventurers meet in the 1930s.
In an exquisite montage, we watch Carl and Ellie grow up, fall in love, and settle down. But their fairytale romance includes some very real heartbreaks, and unfulfilled dreams.
Saddest of all is the day Carl (voiced by Ed Asner) is left a widower. To honor his beloved, he decides to visit South America, as they'd always planned.
Carl travels by air, and like the rest of the beautifully-rendered 3-D images, his takeoff is worth your ticket price alone. Affixing thousands of multihued balloons to his house, he lifts off and enters the sky, only to discover a stowaway: Russell (Jordan Nagai), a neglected 8-year-old who could use a father figure.
Being a cranky loner, Carl has no use for company. Russell, however, isn't the kind of kid to take a hint. Once in South America, they're beset by a pack of furious dogs hunting a rare, ostrich-like bird.
The dogs belong to Charles Muntz (Christopher Plummer), an explorer obsessively determined to capture the bird. As soon as you see this adorable creature, you'll understand why Russell refuses to let that happen.
Filmmakers Pete Docter and Bob Peterson find a near-ideal balance between sweetness and sentimentality (though parents should definitely take that PG rating to heart).
Every detail, from the hairs on Carl's chin to the lovely, lingering score, has been tended to by a Pixar perfectionist.
If there's any oversight, it's that girls will find themselves curiously underrepresented in yet another of Pixar's grand adventures.
The company does such an extraordinary job making movies for everyone. Shouldn't they be about everyone, too?
