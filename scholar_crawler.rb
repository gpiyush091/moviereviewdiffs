require "open-uri"
require 'rubygems'
require 'mechanize'

line_num=0
File.open('mine_1st.txt').each(sep="\n") do |line|
	author_name = line.slice(0..line.length-2)
	file_name = author_name
	file_path = 'data10000/'+file_name
	print author_name
	agent = Mechanize.new
	agent.get("http://scholar.google.com/") do |page|
		search_result = page.form_with(:name => 'f') do |search|
			search.q = author_name
		end.submit
	
		search_result.links.each do |link|
			if link.text .eql? author_name
				break
			end
		end
		if l = search_result.link_with(:text => author_name)
			author_profile = l.click
		else
			next
		end
		file = open(file_path+'.html','w')
		file.write(author_profile.body)
		file.close

		i=2
		while author_profile.link_with(:text => 'Next >') do
			author_profile = author_profile.link_with(:text => 'Next >').click
			file = open(file_path+'-'+i.to_s+'.html','w')
			file.write(author_profile.body)
			file.close
			i+=1
			sleep 5
		end
		break
	end
	line_num+=1
	print line_num
	sleep 10
end