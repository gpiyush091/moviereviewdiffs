import nltk.data
import re

def sentence_tokenize(text):
    sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
    return sent_detector.tokenize(text)
    
def get_words(text):
    words = "\w+(?:-\w+)?(?:'\w+)?"
    return re.findall(words, text)

def get_features_54_to_62(text):
    """
    Gets the following features:
    54. Total number of words (M)
    55. Total number of short words (less than four characters)/M e.g., and, or
    56. Total number of characters in words/C
    57. Average word length
    58. Average sentence length in terms of character
    59. Average sentence length in terms of word
    60. Total different words/M
    61. Hapax legomena* Frequency of once-occurring words
    62. Hapax dislegomena* Frequency of twice-occurring words

    Returns the features in order
    """
    word_list = get_words(text)
    sentence_list = sentence_tokenize(text)

    word_count = len(word_list)
    short_words = 0
    avg_word_length = 0.0
    percent_chars_in_words = 0.0

    frequencies = {}
    for word in word_list:
        if len(word) < 4:
            short_words += 1
        avg_word_length += len(word)
        percent_chars_in_words += len(word)

        if word not in frequencies:
            frequencies[word] = 1
        else:
            frequencies[word] += 1

    avg_word_length /= word_count
    percent_chars_in_words /= len(text)
    different_words = len(frequencies)

    hapax_legomena = 0
    hapax_dislegomena = 0
    for word in frequencies:
        if frequencies[word] == 1:
            hapax_legomena += 1
        if frequencies[word] == 2:
            hapax_dislegomena += 1

    avg_sentence_length_chars = float(len(text)) / len(sentence_list)
    avg_sentence_length_words = float(len(word_list)) / len(sentence_list)

    return word_count, short_words, percent_chars_in_words, avg_word_length, avg_sentence_length_chars, avg_sentence_length_words, different_words, hapax_legomena, hapax_dislegomena

if __name__ == "__main__":
    text = "I absolutely love both of the Hunger Games movies and cannot wait for the third and final one to be released!  Catching Fire has an awesome twist to it, and you never see it coming!  It was a long movie, but definitely worth it to buy and watch over and over :)."
    features = get_features_54_to_62(text)
    print "Total number of words:", features[0]
    print "Total number of short words / M:", features[1]
    print "Total number of characters in words / C:", features[2]
    print "Average word length:", features[3]
    print "Average sentence length in terms of character:", features[4]
    print "Average sentence length in terms of word:", features[5]
    print "Total different words / M:", features[6]
    print "Hapax legomena:", features[7]
    print "Hapax dislegomena:", features[8]
